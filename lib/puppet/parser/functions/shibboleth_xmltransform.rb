module Puppet::Parser::Functions
  newfunction(:shibboleth_xmltransform, :type => :rvalue) do |args|
    name = args[0]
    template_name = args[1]
    mod = Puppet::Module.find('shibboleth', self.compiler.environment.to_s)

    command = [
      '/usr/bin/shibboleth-xmltransform',
      File.join(mod.path, 'xmltemplates', template_name),
      name,
    ]

    facts_json = PSON.dump(self.to_hash)
    buf = IO.popen(command.join(' '), 'w+') do |pipe|
      pipe.write facts_json
      pipe.close_write
      pipe.read.chomp
    end
    exit_status = $?
    unless exit_status == 0
      raise Puppet::ExecutionFailure, "xmltransform: #{name} exited with non-zero status"
    end

    buf
  end
end
