module Puppet::Parser::Functions
  newfunction(:shibboleth_certdata, :type => :rvalue) do |args|
    data = args[0]
    command = [
      '/usr/bin/openssl', 'x509',
    ]
    buf = IO.popen(command.join(' '), mode='w+') do |pipe|
      pipe.write data
      pipe.close_write
      lines = pipe.readlines
      lines.unshift "\n"
      lines.reject { |line| line.start_with? '-----' }.join
    end
    buf
  end
end
