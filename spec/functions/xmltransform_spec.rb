require 'spec_helper'

describe 'xmltransform' do
  it { should run.with_params('reying-party.py', 'relying-party.xml') }
  it { should run.with_params('attribute-filter.py', 'attribute-filter.xml') }
end
