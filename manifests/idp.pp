class shibboleth::idp(
  $salt,
  $cert_pem,
  $key_pem,
  $entity_id="http://${::fqdn}/idp/shibboleth",
  $endpoint="https://${::fqdn}/idp/profile/",
  $confdir='/etc/shibboleth-idp',
  $statedir='/var/lib/shibboleth-idp',
  $ldapurl='ldap://localhost',
  $ldapbasedn='dc=example,dc=domain',
  $metadata_paths = [],
) {
  include tomcat::service

  package { 'shibboleth-idp': ensure => installed }

  $idp_metadata = "${statedir}/metadata/idp-metadata.xml"
  $credential_dir = "${statedir}/credentials"
  $certpath = "${credential_dir}/cert.pem"
  $keypath  = "${credential_dir}/key.pem"
  $certdata = shibboleth_certdata($cert_pem)

  File { owner => 0, group => 0, mode => 0444,
    require => Package['shibboleth-idp'],
    notify  => Service['tomcat'],
  }

  file {
    "${credential_dir}": ensure => directory, recurse => true, purge => true;
    "${certpath}": content => $cert_pem;
    "${keypath}":
      content => $key_pem,
      group   => 'tomcat6',
      mode    => 0440;
  }

  $file_paths = {
    'service'            => "${confdir}/service.xml",
    'attribute-filter'   => "${confdir}/attribute-filter.xml",
    'attribute-resolver' => "${confdir}/attribute-resolver.xml",
    'handler'            => "${confdir}/handler.xml",
    'relying-party'      => "${confdir}/relying-party.xml",
    'login-config'       => "${confdir}/login.config",
    'idp-metadata'       => "${statedir}/metadata/idp-metadata.xml",
  }

  file {
    $file_paths['service']:
      content => shibboleth_xmltransform('service', 'service.xml');
    $file_paths['attribute-filter']:
      content => shibboleth_xmltransform('attribute_filter', 'attribute-filter.xml');
    $file_paths['attribute-resolver']:
      content => shibboleth_xmltransform('attribute_resolver', 'attribute-resolver.xml');
    $file_paths['handler']:
      content => shibboleth_xmltransform('handler', 'handler.xml');
    $file_paths['relying-party']:
      content => shibboleth_xmltransform('relying_party', 'relying-party.xml');
    $file_paths['login-config']:
      content => template('shibboleth/login.config.erb');
    $file_paths['idp-metadata']:
      content => shibboleth_xmltransform('idp_metadata', 'idp-metadata.xml');
  }
}
