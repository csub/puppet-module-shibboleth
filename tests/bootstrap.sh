#!/bin/sh

set -o nounset
set -o errexit

apt-get --quiet -y install build-essential python-dev python-pip libxml2-dev libxslt1-dev git
pip install virtualenv
virtualenv /virtualenv
/virtualenv/bin/pip install git+ssh://git@bitbucket.org/csub/shibboleth-xmltransform#egg=xmltransform
ln -sf /virtualenv/bin/shibboleth-xmltransform /usr/bin/shibboleth-xmltransform

exit 0
