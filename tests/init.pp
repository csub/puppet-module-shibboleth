class profile {
  class { 'shibboleth::idp':
    salt => '0123456789abcdef',
    key_pem => file('/vagrant/tests/key.pem'),
    cert_pem => file('/vagrant/tests/cert.pem'),
    ldapurl => 'ldap://ldap.csub.edu',
    ldapbasedn => 'dc=csub,dc=edu',
  }
}

class aptconf {
  include debian::apt::source::csub_stable
}

stage { 'pre': before => Stage['main'] }
class { 'apt': stage => 'pre' }
class { 'aptconf': stage => 'pre' }
class { 'profile': stage => 'main' }
